import express,{ Express,Request,Response } from "express";
import dotenv from 'dotenv'

// Configuration the .env file
dotenv.config()

// Create Express APP
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

// Define the first route of APP

app.get('/', (req: Request, res: Response) => {
    // Send Hello world
    res.send('Node JS');
})

app.get('/hello', (req: Request, res: Response) => {
    // Send Hello world
    res.send('Hello World!');
})

// Excute APP and listen on port
app.listen(port, () => {
    console.log(`Express server listening in: http://localhost:${port}`);
});
